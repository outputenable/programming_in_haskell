-- 7.8 Exercises

import Data.Char (chr, ord)
import Prelude hiding (
  all,
  any,
  curry,
  dropWhile,
  filter,
  iterate,
  map,
  takeWhile,
  uncurry,
  )

-- 1.
mapfilt :: (a -> b) -> (a -> Bool) -> [a] -> [b]
mapfilt f p = map f . filter p

-- 2.
all :: (a -> Bool) -> [a] -> Bool
all p = and . map p

any :: (a -> Bool) -> [a] -> Bool
any p = or . map p

takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile _ []                 = []
takeWhile p (x:xs) | p x       = x:takeWhile p xs
                   | otherwise = []

dropWhile :: (a -> Bool) -> [a] -> [a]
dropWhile _ []                 = []
dropWhile p (x:xs) | p x       = dropWhile p xs
                   | otherwise = x:xs

-- 3.
map :: (a -> b) -> [a] -> [b]
map f = foldr (\x xs -> f x:xs) []

filter :: (a -> Bool) -> [a] -> [a]
filter p = foldr (\x xs -> if p x then x:xs else xs) []

-- 4.
dec2int :: [Int] -> Int
dec2int = foldl (\x y -> 10 * x + y) 0

-- 5.
-- from 7.5 The composition operator
compose :: [a -> a] -> (a -> a)
compose = foldr (.) id

-- sumsqreven = compose [sum, map (^2), filter even]
-- is invalid because sum :: [b] -> b but compose expects type a -> a.

-- 6.
curry :: ((a, b) -> c) -> (a -> b -> c)
curry f = \x y -> f (x, y)

uncurry :: (a -> b -> c) -> ((a, b) -> c)
uncurry f = \(x, y) -> f x y

-- 7.
unfold p h t x | p x = []
               | otherwise = h x:unfold p h t (t x)

-- see 7.6 String transmitter
int2bin = unfold (== 0) (`mod` 2) (`div` 2)

type Bit = Int

chop8 :: [Bit] -> [[Bit]]
chop8 = unfold null (take 8) (drop 8)

map' :: (a -> b) -> [a] -> [b]
map' f = unfold null (\xs -> f (head xs)) tail

iterate f = unfold (\_ -> False) id f

-- 8. + 9.
bin2int :: [Bit] -> Int
bin2int = foldr (\x y -> x + 2 * y) 0

make8 :: [Bit] -> [Bit]
make8 bits = take 8 (bits ++ repeat 0)

parity :: [Bit] -> Bit
parity = (`mod` 2) . sum

makeEvenParity :: [Bit] -> [Bit]
makeEvenParity bits = bits ++ [parity bits]

encode :: String -> [Bit]
encode = concat . map (makeEvenParity . make8 . int2bin . ord)

chop :: Int -> [Bit] -> [[Bit]]
chop _ []   = []
chop n bits = take n bits:chop n (drop n bits)

checkEvenParity :: [Bit] -> [Bit]
checkEvenParity bits | even (parity bits) = init bits
                     | otherwise          = error "Parity error"

decode :: [Bit] -> String
decode = map (chr . bin2int . checkEvenParity) . (chop 9)

transmit :: String -> String
transmit = decode . channel . encode

channel :: [Bit] -> [Bit]
channel = tail
