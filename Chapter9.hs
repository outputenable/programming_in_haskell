-- 9.9 Exercises

-- 1.
delete :: IO ()
delete = putStr "\ESC[1D \ESC[1D"

readLine' :: IO String
readLine' = do x <- getChar
               if x == '\n' then
                 return []
               else
                 if x == '\DEL' then
                   -- Hitting backspace prints "^?",
                   -- should probably use getCh from §9.4!?
                   do delete; delete
                      return ['\DEL']
                 else
                   do xs <- readLine'
                      if not (null xs) && head xs == '\DEL' then
                        do delete
                           xs <- readLine'
                           return xs
                      else
                        return (x:xs)

readLine :: IO String
readLine = do xs <- readLine'
              case xs of
                ['\DEL']  -> readLine
                otherwise -> return xs
