-- 8.10 Exercises

import Parsing hiding (int)

-- 1.
int :: Parser Int
int = do char '-'
         n <- nat
         return (-n)
      +++ nat

-- 2.
comment :: Parser ()
comment = do string "--"
             many (sat (/= '\n'))
             char '\n'
             return ()

-- 3.
{-
          _expr_                _expr_
         /  |   \              /  |   \
        /   +    \            /   +    \
     expr        expr      expr        expr
    / |  \        |         |         / |  \
   /  +   \      term      term      /  +   \
 expr    expr     |         |      expr    expr
  |       |     factor    factor    |       |
 term    term     |         |      term    term
  |       |      nat       nat      |       |
factor  factor    |         |     factor  factor
  |       |       4         2       |       |
 nat     nat                       nat     nat
  |       |                         |       |
  2       3                         3       4
-}

-- 4.
{-
2 + 3:

     expr
    / |  \
   /  +   \
 term    expr
  |       |
factor   term
  |       |
 nat    factor
  |       |
  2      nat
          |
          3

2 * 3 * 4:

      expr
       |
     _term_
    /  |   \
   /   *    \
factor      term
  |        / |  \
 nat      /  *   \
  |    factor   term
  2      |       |
        nat    factor
         |       |
         3      nat
                 |
                 4

(2 + 3) + 4:

         _expr_
        /  |   \
       /   +    \
     term       expr
      |          |
    factor      term
      |          |
    (expr)     factor
    / | \        |
   /  +  \      nat
 term    expr    |
  |       |      4
factor   term
  |       |
 nat    factor
  |       |
  2      nat
          |
          3
-}
