-- n+k patterns were apparently removed in Haskell 2010; see
-- https://stackoverflow.com/questions/3748592/what-are-nk-patterns-and-why-are-they-banned-from-haskell-2010
-- for a compact description.  Will nevertheless use them to preserve the
-- original vibe.
{-# LANGUAGE NPlusKPatterns #-}

import Prelude hiding (
  (!!),
  (^),
  and,
  concat,
  elem,
  init,
  last,
  replicate,
  sum,
  take,
  )

init :: [a] -> [a]
init [_]    = []
init (x:xs) = x:init xs

-- 6.8 Exercises

-- 1.
(^) :: Int -> Int -> Int
_ ^ 0     = 1
m ^ (n+1) = m * (m ^ n)

--   2^3
-- =    { applying ^ }
--   2 * (2 ^ 2)
-- =    { applying ^ }
--   2 * (2 * (2 ^ 1))
-- =    { applying ^ }
--   2 * (2 * (2 * (2 ^ 0)))
-- =    { applying ^ }
--   2 * (2 * (2 * (1)))
-- =    { applying * }
--   8

-- 3.
and :: [Bool] -> Bool
and []     = True
and (x:xs) = x && and xs

concat :: [[a]] -> [a]
concat []       = []
concat (xs:xss) = xs ++ concat xss

replicate :: Int -> a -> [a]
replicate 0 _ = []
replicate (n+1) x = x:replicate n x

(!!) :: [a] -> Int -> a
(x:_) !! 0      = x
(_:xs) !! (n+1) = xs !! n

elem :: Eq a => a -> [a] -> Bool
elem _ []     = False
elem x (y:ys) = x == y || elem x ys

-- 4.
merge :: Ord a => [a] -> [a] -> [a]
merge xs []                     = xs
merge [] ys                     = ys
merge (x:xs) (y:ys) | x < y     = x:merge xs (y:ys)
                    | otherwise = y:merge (x:xs) ys

-- 5.
halve :: [a] -> ([a], [a])
halve xs = splitAt ((length xs) `div` 2) xs

msort :: Ord a => [a] -> [a]
msort []  = []
msort [x] = [x]
msort xs  = merge (msort ls) (msort rs)
            where (ls, rs) = halve xs

-- 6.
sum :: Num a => [a] -> a
sum []     = 0
sum (x:xs) = x + sum xs

take :: Int -> [a] -> [a]
take 0 _          = []
take (n+1) (x:xs) = x:take n xs

last :: [a] -> a
last [x]    = x
last (_:xs) = last xs
