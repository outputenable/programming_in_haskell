-- 4.8 Exercises

-- 1.
halve :: [a] -> ([a], [a])
halve xs = splitAt (length xs `div` 2) xs

-- 2.
-- (a)
safetailCond :: [a] -> [a]
safetailCond xs = if null xs then [] else tail xs
-- (b)
safetailGuarded :: [a] -> [a]
safetailGuarded xs | null xs   = []
                   | otherwise = tail xs
-- (c)
safetailPattern :: [a] -> [a]
safetailPattern []     = []
safetailPattern (x:xs) = xs

-- 3.
disjunctA :: Bool -> Bool -> Bool
disjunctA False False = False
disjunctA False True  = True
disjunctA True False  = True
disjunctA True True   = True

disjunctB :: Bool -> Bool -> Bool
disjunctB False False = False
disjunctB _ _         = True

disjunctC :: Bool -> Bool -> Bool
disjunctC False b = b
disjunctC True _  = True

disjunctD :: Bool -> Bool -> Bool
disjunctD a b | a == b    = b
              | otherwise = True

-- 4.
conjunctA :: Bool -> Bool -> Bool
conjunctA a b = if a then if b then True else False else False

-- 5.
conjunctB :: Bool -> Bool -> Bool
conjunctB a b = if a then b else False

-- 6.
mult :: Num a => a -> a -> a -> a
mult = \x -> (\y -> (\z -> x * y * z))
