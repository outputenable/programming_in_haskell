-- 8.10 Exercises

-- 8.
-- (a)
{-
expr ::= expr - nat | nat
nat  ::= 0|1|2|...
-}

-- (b)
import Parsing

expr :: Parser Int
expr = do e <- expr
          symbol "-"
          n <- natural
          return (e - n)
        +++ natural

eval :: String -> Int
eval xs = case parse expr xs of
            [(n, [])]  -> n
            [(_, out)] -> error ("unused input " ++ out)
            []         -> error "invalid input"

-- (c)
-- *** Exception: stack overflow

-- (d)
-- cf. the example parser for a non-empty list of natural numbers in §8.7
expr' :: Parser Int
expr' = do n <- natural
           ns <- many (do symbol "-"
                          natural)
           return (foldl (-) n ns)

eval' :: String -> Int
eval' xs = case parse expr' xs of
             [(n, [])]  -> n
             [(_, out)] -> error ("unused input " ++ out)
             []         -> error "invalid input"
